ScpAdminReports adds a possibility to add your ban/kick logs to your Discord server! Simply download a release from the [releases](https://sinsa92.net/sinsa-plugins/scpadminreports/-/releases) page (I highly recommend 1.3.0, as it's the latest stable right now), and then put the .dll into your plugins folder.

This is a plugin made specifically for EXILED 2.0.10. Newer versions might work, but it is not guaranteed to work. EXILED 3 will most likely break it.

This plugin will not be updated as of now, maybe I will update it in the future.