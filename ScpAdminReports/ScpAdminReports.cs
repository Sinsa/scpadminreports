﻿using System;
using Exiled.API.Enums;
using Exiled.API.Features;

using Server = Exiled.Events.Handlers.Server;
using Player = Exiled.Events.Handlers.Player;

namespace ScpAdminReports
{
    public sealed class ScpAdminReports : Plugin<Config>
    {
        private static readonly Lazy<ScpAdminReports> LazyInstance =
            new Lazy<ScpAdminReports>(() => new ScpAdminReports());
        public static ScpAdminReports Instance => LazyInstance.Value;

        public override PluginPriority Priority { get; } = PluginPriority.Medium;

        private Handlers.Player player;

        private ScpAdminReports()
        {
            
        }

        public override void OnEnabled()
        {
            RegisterEvents();
        }

        public override void OnDisabled()
        {
            UnregisterEvents();
        }

        public void RegisterEvents()
        {
            player = new Handlers.Player();

            Player.Kicking += player.OnPlayerKick;
            Player.Banning += player.OnPlayerBan;
        }

        public void UnregisterEvents()
        {

            Player.Kicking -= player.OnPlayerKick;
            Player.Banning -= player.OnPlayerBan;

            player = null;
        }
    }
}