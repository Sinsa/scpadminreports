﻿using Exiled.API.Features;
using Exiled.Events.EventArgs;
using Newtonsoft.Json;

namespace ScpAdminReports.Handlers
{
    public class Player
    {
        private DiscordEmbedBuilder _embedBuilder = new DiscordEmbedBuilder();
        private DiscordWebHook WebHook = new DiscordWebHook();
        public void OnPlayerKick(KickingEventArgs eventArgs)
        {
            if (ScpAdminReports.Instance.Config.KickEnabled)
            {
                _embedBuilder.Description = "";
                int maxfiller = setMaxFiller();

                if (ScpAdminReports.Instance.Config.NameEnabled)
                {
                    string filler = fillFiller(maxfiller, ScpAdminReports.Instance.Config.NameLabel.Length);
                    _embedBuilder.Description +=
                        $"``` {ScpAdminReports.Instance.Config.NameLabel}{filler}:   {eventArgs.Target.Nickname}";
                }

                if (ScpAdminReports.Instance.Config.IdEnabled)
                {
                    string filler = fillFiller(maxfiller, ScpAdminReports.Instance.Config.IdLabel.Length);
                    if (!ScpAdminReports.Instance.Config.IdSuffixEnabled)
                    {
                        string loggingId = eventArgs.Target.UserId.Substring(0, eventArgs.Target.UserId.IndexOf("@"));
                        _embedBuilder.Description +=
                            $"\n {ScpAdminReports.Instance.Config.IdLabel}{filler}:   {loggingId}";
                    }
                    else
                    {
                        _embedBuilder.Description +=
                            $"\n {ScpAdminReports.Instance.Config.IdLabel}{filler}:   {eventArgs.Target.UserId}";
                    }
                }

                if (ScpAdminReports.Instance.Config.IpEnabled)
                {
                    string filler = fillFiller(maxfiller, ScpAdminReports.Instance.Config.IpLabel.Length);
                    _embedBuilder.Description +=
                        $"\n {ScpAdminReports.Instance.Config.IpLabel}{filler}:   {eventArgs.Target.IPAddress}";
                }

                if (ScpAdminReports.Instance.Config.ActionEnabled)
                {
                    string filler = fillFiller(maxfiller, ScpAdminReports.Instance.Config.ActionLabel.Length);
                    _embedBuilder.Description += $"\n {ScpAdminReports.Instance.Config.ActionLabel}{filler}:   kick";
                }

                if (ScpAdminReports.Instance.Config.ReasonEnabled)
                {
                    string filler = fillFiller(maxfiller, ScpAdminReports.Instance.Config.ReasonLabel.Length);
                    _embedBuilder.Description +=
                        $"\n {ScpAdminReports.Instance.Config.ReasonLabel}{filler}:   {eventArgs.Reason}";
                }

                if (ScpAdminReports.Instance.Config.IssuerEnabled)
                {
                    string filler = fillFiller(maxfiller, ScpAdminReports.Instance.Config.IssuerLabel.Length);
                    if (!ScpAdminReports.Instance.Config.IdSuffixEnabled)
                    {
                        string loggingId = eventArgs.Issuer.UserId.Substring(0, eventArgs.Issuer.UserId.IndexOf("@"));
                        _embedBuilder.Description +=
                            $"\n {ScpAdminReports.Instance.Config.IssuerLabel}{filler}:   {eventArgs.Issuer.Nickname} ({loggingId})```";
                    }
                    else
                    {

                        _embedBuilder.Description +=
                            $"\n {ScpAdminReports.Instance.Config.IssuerLabel}{filler}:   {eventArgs.Issuer.Nickname} ({eventArgs.Issuer.UserId})```";
                    }
                }

                if (ScpAdminReports.Instance.Config.NameEnabled || ScpAdminReports.Instance.Config.IdEnabled ||
                    ScpAdminReports.Instance.Config.IpEnabled || ScpAdminReports.Instance.Config.ActionEnabled ||
                    ScpAdminReports.Instance.Config.ReasonEnabled || ScpAdminReports.Instance.Config.IssuerEnabled)
                {
                    string jsonResult = JsonConvert.SerializeObject(_embedBuilder);
                    WebHook.SendMessage(jsonResult, eventArgs.Target.ReferenceHub.serverRoles.DoNotTrack);
                    Log.Info(
                        $"{eventArgs.Target.Nickname} ({eventArgs.Target.UserId}) was kicked by {eventArgs.Issuer.Nickname} ({eventArgs.Issuer.UserId}).");
                }
            }
        }

        public void OnPlayerBan(BanningEventArgs eventArgs)
        {
            if (ScpAdminReports.Instance.Config.BansEnabled)
            {
                _embedBuilder.Description = "";
                int maxfiller = setMaxFiller();

                if (ScpAdminReports.Instance.Config.NameEnabled)
                {
                    string filler = fillFiller(maxfiller, ScpAdminReports.Instance.Config.NameLabel.Length);
                    _embedBuilder.Description +=
                        $"``` {ScpAdminReports.Instance.Config.NameLabel}{filler}:   {eventArgs.Target.Nickname}";
                }

                if (ScpAdminReports.Instance.Config.IdEnabled)
                {
                    string filler = fillFiller(maxfiller, ScpAdminReports.Instance.Config.IdLabel.Length);
                    if (!ScpAdminReports.Instance.Config.IdSuffixEnabled)
                    {
                        string loggingId = eventArgs.Target.UserId.Substring(0, eventArgs.Target.UserId.IndexOf("@"));
                        _embedBuilder.Description +=
                            $"\n {ScpAdminReports.Instance.Config.IdLabel}{filler}:   {loggingId}";
                    }
                    else
                    {
                        _embedBuilder.Description +=
                            $"\n {ScpAdminReports.Instance.Config.IdLabel}{filler}:   {eventArgs.Target.UserId}";
                    }
                }

                if (ScpAdminReports.Instance.Config.IpEnabled)
                {
                    string filler = fillFiller(maxfiller, ScpAdminReports.Instance.Config.IpLabel.Length);
                    _embedBuilder.Description +=
                        $"\n {ScpAdminReports.Instance.Config.IpLabel}{filler}:   {eventArgs.Target.IPAddress}";
                }

                if (ScpAdminReports.Instance.Config.ActionEnabled)
                {
                    string filler = fillFiller(maxfiller, ScpAdminReports.Instance.Config.ActionLabel.Length);
                    _embedBuilder.Description +=
                        $"\n {ScpAdminReports.Instance.Config.ActionLabel}{filler}:   ban ({formatTime(eventArgs.Duration)})";
                }

                if (ScpAdminReports.Instance.Config.ReasonEnabled)
                {
                    string filler = fillFiller(maxfiller, ScpAdminReports.Instance.Config.ReasonLabel.Length);
                    _embedBuilder.Description +=
                        $"\n {ScpAdminReports.Instance.Config.ReasonLabel}{filler}:   {eventArgs.Reason}";
                }

                if (ScpAdminReports.Instance.Config.IssuerEnabled)
                {
                    string filler = fillFiller(maxfiller, ScpAdminReports.Instance.Config.IssuerLabel.Length);
                    if (!ScpAdminReports.Instance.Config.IdSuffixEnabled)
                    {
                        string loggingId = eventArgs.Issuer.UserId.Substring(0, eventArgs.Issuer.UserId.IndexOf("@"));
                        _embedBuilder.Description +=
                            $"\n {ScpAdminReports.Instance.Config.IssuerLabel}{filler}:   {eventArgs.Issuer.Nickname} ({loggingId})```";
                    }
                    else
                    {

                        _embedBuilder.Description +=
                            $"\n {ScpAdminReports.Instance.Config.IssuerLabel}{filler}:   {eventArgs.Issuer.Nickname} ({eventArgs.Issuer.UserId})```";
                    }
                }

                string jsonResult = JsonConvert.SerializeObject(_embedBuilder);
                WebHook.SendMessage(jsonResult, eventArgs.Target.ReferenceHub.serverRoles.DoNotTrack);
                Log.Info(
                    $"{eventArgs.Target.Nickname} ({eventArgs.Target.UserId})was banned by {eventArgs.Issuer.Nickname} ({eventArgs.Issuer.UserId}) for {formatTime(eventArgs.Duration)}.");
            }
        }

        private string formatTime(int time)
        {
            if (time < 60)
            {
                return($"{time}s");
            }
            
            if (time < 7200)
            {
                int newtime = (time + 59)/60;
                return ($"{newtime}min");
            }

            if (time < 129600)
            {
                double newtime = (time + 3599) / 3600;
                string newtimestring;
                if (newtime.ToString().Length > 2)
                {
                    newtimestring = newtime.ToString().Substring(0, 3);
                }
                else
                {
                    newtimestring = newtime.ToString();
                }
                return ($"{newtimestring}h");
            }

            if (time < 2678400)
            {
                double newtime = time / 86400;
                string newtimestring;
                if (newtime.ToString().Length > 2)
                {
                    newtimestring = newtime.ToString().Substring(0, 3);
                }
                else
                {
                    newtimestring = newtime.ToString();
                }
                return ($"{newtimestring}d");
            }

            if (time < 31622400)
            {
                double newtime = time / 2592000;
                string newtimestring;
                if (newtime.ToString().Length > 2)
                {
                    newtimestring = newtime.ToString().Substring(0, 3);
                }
                else
                {
                    newtimestring = newtime.ToString();
                }

                return ($"{newtimestring}mon");
            }
            double newtimeoutsidethefuckingconditions = time / 31536000;
            string newtimeoutsidethefuckingconditionsstring;
            if (newtimeoutsidethefuckingconditions.ToString().Length > 2)
            {
                newtimeoutsidethefuckingconditionsstring = newtimeoutsidethefuckingconditions.ToString().Substring(0, 3);
            }
            else
            {
                newtimeoutsidethefuckingconditionsstring = newtimeoutsidethefuckingconditions.ToString();
            }

            return ($"{newtimeoutsidethefuckingconditionsstring}y");
        }

        private string fillFiller(int maxFiller, int strLength)
        {
            string filler = "";
            while (filler.Length < maxFiller - strLength)
            {
                filler += " ";
            }
            return filler;
        }

        private int setMaxFiller()
        {
            int maxfiller = 12;
            if (ScpAdminReports.Instance.Config.NameLabel.Length > maxfiller && ScpAdminReports.Instance.Config.NameEnabled)
            {
                maxfiller = ScpAdminReports.Instance.Config.NameLabel.Length + 1;
            }
            if (ScpAdminReports.Instance.Config.IdLabel.Length > maxfiller && ScpAdminReports.Instance.Config.IdEnabled)
            {
                maxfiller = ScpAdminReports.Instance.Config.IdLabel.Length + 1;
            }
            if (ScpAdminReports.Instance.Config.IpLabel.Length > maxfiller && ScpAdminReports.Instance.Config.IpEnabled)
            {
                maxfiller = ScpAdminReports.Instance.Config.IpLabel.Length + 1;
            }
            if (ScpAdminReports.Instance.Config.ActionLabel.Length > maxfiller && ScpAdminReports.Instance.Config.ActionEnabled)
            {
                maxfiller = ScpAdminReports.Instance.Config.ActionLabel.Length + 1;
            }
            if (ScpAdminReports.Instance.Config.ReasonLabel.Length > maxfiller && ScpAdminReports.Instance.Config.ReasonEnabled)
            {
                maxfiller = ScpAdminReports.Instance.Config.ReasonLabel.Length + 1;
            }
            if (ScpAdminReports.Instance.Config.IssuerLabel.Length > maxfiller && ScpAdminReports.Instance.Config.IssuerEnabled)
            {
                maxfiller = ScpAdminReports.Instance.Config.IssuerLabel.Length + 1;
            }

            return maxfiller;
        }
    }
}