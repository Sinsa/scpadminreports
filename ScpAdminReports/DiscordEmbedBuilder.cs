﻿namespace ScpAdminReports
{
    public class DiscordEmbedBuilder
    {
        public string Title = ScpAdminReports.Instance.Config.WebHookEmbedTitle;
        public string Type = "rich";
        public int Color = ScpAdminReports.Instance.Config.WebHookEmbedColor;
        public string Description;
    }
}