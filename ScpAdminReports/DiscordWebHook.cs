﻿using System;
using System.IO;
using System.Net;
using Exiled.API.Features;

namespace ScpAdminReports
{
    public class DiscordWebHook : IDisposable
    {
        //private readonly WebClient _discordWebClient;
        private HttpWebRequest _discordWebRequest;
        public string DNTWebHook = ScpAdminReports.Instance.Config.DntWebHookUrl;
        public string WebHook = ScpAdminReports.Instance.Config.WebHookUrl;
        public string UserName = ScpAdminReports.Instance.Config.WebHookUsername;
        public string ProfilePicture = ScpAdminReports.Instance.Config.WebHookAvatarUrl;

        public DiscordWebHook()
        {
        }

        public void SendMessage(string message, bool doNotTrackActive)
        {
            if (doNotTrackActive)
            {
                _discordWebRequest = (HttpWebRequest) WebRequest.Create(DNTWebHook);
                _discordWebRequest.ContentType = "application/json";
                _discordWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(_discordWebRequest.GetRequestStream()))
                {
                    string payload = "{\"username\":\"" + UserName + "\",\"avatar_url\":\"" + ProfilePicture +
                                     "\",\"embeds\":[" + message + "]}";
                    streamWriter.Write(payload);
                }

                Log.Debug((HttpWebResponse) _discordWebRequest.GetResponse());
            }
            else
            {
                _discordWebRequest = (HttpWebRequest) WebRequest.Create(WebHook);
                _discordWebRequest.ContentType = "application/json";
                _discordWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(_discordWebRequest.GetRequestStream()))
                {
                    string payload = "{\"username\":\"" + UserName + "\",\"avatar_url\":\"" + ProfilePicture +
                                     "\",\"embeds\":[" + message + "]}";
                    streamWriter.Write(payload);
                }

                Log.Debug((HttpWebResponse) _discordWebRequest.GetResponse());
            }
        }
        public void Dispose()
        {
            _discordWebRequest.EndGetRequestStream(null);
        }
    }
}