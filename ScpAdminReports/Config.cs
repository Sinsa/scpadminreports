﻿using System.ComponentModel;
using Exiled.API.Interfaces;

namespace ScpAdminReports
{
    public class Config : IConfig
    {
        public bool IsEnabled { get; set; } = false;

        [Description("Should Kicks be logged")]
        public bool KickEnabled { get; set; } = true;

        [Description("Should Bans be logged")]
        public bool BansEnabled { get; set; } = true;

        [Description("The Username of the Webhook")]
        public string WebHookUsername { get; set; } = "SCP Reports";

        [Description("The Title of the Embed")]
        public string WebHookEmbedTitle { get; set; } = "SCP Reports";

        [Description("The Color of the Embed. Must be set in Decimal format. For a random Color enter 'RANDOM'.")]
        public int WebHookEmbedColor { get; set; } = 16711680;
        
        [Description("The Avatar of the Webhook")]
        public string WebHookAvatarUrl { get; set; } = "https://discordapp.com/assets/1cbd08c76f8af6dddce02c5138971129.png";
        
        [Description("The URL of the Webhook")]
        public string WebHookUrl { get; set; } = "https://discordapp.com/api/webhooks/WEBHOOK";
        
        [Description("The URL of the Webhook where logs with a player that has DoNotTrack on should be sent to")]
        public string DntWebHookUrl { get; set; } = "https://discordapp.com/api/webhooks/WEBHOOK";

        [Description("Should the Nickname be included in the Report")]
        public bool NameEnabled { get; set; } = true;
        
        [Description("The Label for the Nickname that is shown in the Report")]
        public string NameLabel { get; set; } = "Nickname";

        [Description("Should the ID be included in the Report")]
        public bool IdEnabled { get; set; } = true;

        [Description("Should the ID-suffixes (@steam, @discord or @northwood) be included in the Report")]
        public bool IdSuffixEnabled { get; set; } = true;

        [Description("The Label for the ID that is shown in the Report")]
        public string IdLabel { get; set; } = "ID";

        [Description("Should the IP of the banning Person be included in the Report")]
        public bool IpEnabled { get; set; } = false;

        [Description("The Label for the IP that is shown in the Report")]
        public string IpLabel { get; set; } = "IP";
        
        [Description("Should the Action taken be included in the Report")]
        public bool ActionEnabled { get; set; } = true;

        [Description("The Label for the Action taken that is shown in the Report")]
        public string ActionLabel { get; set; } = "Action";

        [Description("Should the Reason be included in the Report")]
        public bool ReasonEnabled { get; set; } = true;

        [Description("The Label for the Reason that is shown in the Report")]
        public string ReasonLabel { get; set; } = "Reason";

        [Description("Should the Issuing Person be included in the Report")]
        public bool IssuerEnabled { get; set; } = true;

        [Description("The Label for the Issuing Person that is shown in the Report")]
        public string IssuerLabel { get; set; } = "Issuing Staff";
    }
}